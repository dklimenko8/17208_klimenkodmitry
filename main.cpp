#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Figure.h"
#include "Circle.h"
#include "Triangle.h"
#include "Quadrangle.h"
#include "Parallelogram.h"
#include "Trapeze.h"
#include "Rectangle.h"
#include "Rhombus.h"
#include "Square.h"

using namespace std;

Figure* CreateFigure(const std::string& name, const std::string& params) {

    int start = 0, finish = 0, i = 0, k = 1;
    std::string substring;
    float  R, storona1, storona2, storona3, storona4, height;

    if (name == "circle") {
        while (params[i])
            ++i;
        finish = i - 1;

        substring = params.substr(0, finish - start + 1);
        R = std::stof(substring);
        Figure* f = new Circle(R);
        return f;
    }

    if (name == "triangle") {
        while (params[i]){
            if (params[i] == ' '){
                finish = i - 1;
                substring = params.substr(start, finish - start + 1);
                if (k == 2){
                    storona2 =  std::stof(substring);
                    ++k;
                }
                if (k == 1){
                    storona1 =  std::stof(substring);
                    ++k;
                }
                start = finish + 2;
            }
            ++i;
        }
        finish = i - 1;
        substring = params.substr(start, finish);
        storona3 =  std::stof(substring);
        Figure* f = new Triangle(storona1, storona2, storona3);
        return f;
    }

    if (name == "quadrangle") {
        while (params[i]){
            if (params[i] == ' '){
                finish = i - 1;
                substring = params.substr(start, finish - start + 1);
                if (k == 3){
                    storona3 =  std::stof(substring);
                    ++k;
                }
                if (k == 2){
                    storona2 =  std::stof(substring);
                    ++k;
                }
                if (k == 1){
                    storona1 =  std::stof(substring);
                    ++k;
                }
                start = finish + 2;
            }
            ++i;
        }
        finish = i - 1;
        substring = params.substr(start, finish);
        storona4 =  std::stof(substring);
        Figure* f = new Quadrangle(storona1, storona2, storona3, storona4);
        return f;
    }

    if (name == "parallelogram") {
        while (params[i]){
            if (params[i] == ' '){
                finish = i - 1;
                substring = params.substr(start, finish - start + 1);
                if (k == 2){
                    storona2 =  std::stof(substring);
                    ++k;
                }
                if (k == 1){
                    storona1 =  std::stof(substring);
                    ++k;
                }
                start = finish + 2;
            }
            ++i;
        }
        finish = i - 1;
        substring = params.substr(start, finish);
        height =  std::stof(substring);
        Figure* f = new Parallelogram(storona1, storona2, storona1, storona2, height);
        return f;
    }

    if (name == "trapeze") {
        while (params[i]){
            if (params[i] == ' '){
                finish = i - 1;
                substring = params.substr(start, finish - start + 1);
                if (k == 4){
                    storona4 =  std::stof(substring);
                    ++k;
                }
                if (k == 3){
                    storona3 =  std::stof(substring);
                    ++k;
                }
                if (k == 2){
                    storona2 =  std::stof(substring);
                    ++k;
                }
                if (k == 1){
                    storona1 =  std::stof(substring);
                    ++k;
                }
                start = finish + 2;
            }
            ++i;
        }
        finish = i - 1;
        substring = params.substr(start, finish);
        height =  std::stof(substring);
        Figure* f = new Trapeze(storona1, storona2, storona3, storona4, height);
        return f;
    }

    if (name == "rectangle") {
        while (params[i]){
            if (params[i] == ' '){
                finish = i - 1;
                substring = params.substr(start, finish - start + 1);
                storona1 =  std::stof(substring);
                start = finish + 2;
            }
            ++i;
        }
        finish = i - 1;
        substring = params.substr(start, finish);
        storona2 =  std::stof(substring);
        Figure* f = new Rectangle(storona1, storona1, storona2, storona2, storona2);
        return f;
    }

    if (name == "rhombus") {
        while (params[i]){
            if (params[i] == ' '){
                finish = i - 1;
                substring = params.substr(start, finish - start + 1);
                storona1 =  std::stof(substring);
                start = finish + 2;
            }
            ++i;
        }
        finish = i - 1;
        substring = params.substr(start, finish);
        height =  std::stof(substring);
        Figure* f = new Rhombus(storona1, storona1, storona1, storona1, height);
        return f;
    }

    if (name == "square") {
        while (params[i])
            ++i;
        finish = i - 1;
        substring = params.substr(0, finish - start + 1);
        storona1 = std::stof(substring);
        Figure* f = new Square(storona1, storona1, storona1, storona1, storona1);
        return f;
    }

    return nullptr;
}

int main()
{
    std::ifstream file;

    file.open("C:/Users/d.klimenko/Downloads/file.txt");
    if (!file) {
        std::cout << "File is not open\n";
        return 0;
    }

    std::vector<Figure*> figures;



    while (file) {
        std::string line, params;
        std::getline(file, line);
        int n;

        switch (line.substr(0,6)){
            case "circle": {
                n = 6;
                break;
            }
            case "triang": {
                if (line.substr(0,8) == "triangle"){
                    n = 8;
                    break;
                }
                std::cout << "Unknown figure " << line << std::endl;
            }
            case "quadra": {
                if (line.substr(0,10) == "quadrangle"){
                    n = 10;
                    break;
                }
                std::cout << "Unknown figure " << line << std::endl;
            }
            case "parall": {
                if (line.substr(0,13) == "parallelogram"){
                    n = 13;
                    break;
                }
                std::cout << "Unknown figure " << line << std::endl;
            }
            case "trapez": {
                if (line.substr(0,7) == "trapeze"){
                    n = 7;
                    break;
                }
                std::cout << "Unknown figure " << line << std::endl;
            }
            case "rectan": {
                if (line.substr(0,9) == "rectangle"){
                    n = 9;
                    break;
                }
                std::cout << "Unknown figure " << line << std::endl;
            }
            case "rhombu": {
                if (line.substr(0,7) == "rhombus"){
                    n = 7;
                    break;
                }
                std::cout << "Unknown figure " << line << std::endl;
            }
            case "square": {
                if (line.substr(0,6) == "square"){
                    n = 6;
                    break;
                }
                std::cout << "Unknown figure " << line << std::endl;
            }
            default:{
                std::cout << "Unknown figure " << line << std::endl;
            }
    }
        params = line.substr(n + 1);
        Figure* f = CreateFigure(line.substr(0,n), params);
        figures.push_back(f);
    }

    for (auto* f: figures) {
        std::cout <<f->Area()<<" ";
        std::cout << f->Perimetr()<< std::endl;
        delete f;
    }

    return 0;
}
