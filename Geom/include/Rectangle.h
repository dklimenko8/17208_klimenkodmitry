#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Parallelogram.h"

class Rectangle:public Parallelogram
{
    public:
        Rectangle(float a, float b, float c, float d, float h);
        float Area();
        float Get_area();//���������� �������� �������
        void print();//������� �������������� ������ �������� ��������� � �������
    private:
        float area;
        float height;
};

#endif // RECTANGLE_H
