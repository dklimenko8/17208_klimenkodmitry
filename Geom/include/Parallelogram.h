#ifndef PARALLELOGRAM_H
#define PARALLELOGRAM_H

#include "Quadrangle.h"

class Parallelogram:public Quadrangle
{
    public:
        Parallelogram(float a, float b, float c, float d, float h);
        virtual float Area();
        virtual float Get_area();
        virtual void print();
    private:
        float area;
    protected:
        float height;
};

#endif // PARALLELOGRAM_H
