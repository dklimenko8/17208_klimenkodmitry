#ifndef SQUARE_H
#define SQUARE_H

#include "Parallelogram.h"

class Square:public Parallelogram
{
    public:
        Square(float a, float b, float c, float d, float h);
        float Area();
        float Get_area();//���������� �������� �������
        void print();//������� �������������� ������ �������� ��������� � �������
    private:
        float area;
        float height;
};

#endif // SQUARE_H
