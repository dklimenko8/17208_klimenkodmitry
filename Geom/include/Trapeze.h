#ifndef TRAPEZE_H
#define TRAPEZE_H

#include "Quadrangle.h"

class Trapeze:public Quadrangle
{
    public:
        Trapeze(float a, float b, float c, float d, float h);
        float Area();
        float Get_area();//���������� �������� �������
        void print();//������� �������������� ������ �������� ��������� � �������
    private:
        float area;
        float height;
};

#endif // TRAPEZE_H
