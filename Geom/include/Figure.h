#ifndef FIGURE_H
#define FIGURE_H

class Figure
{
    public:
        virtual ~Figure();
        virtual float Perimetr() = 0;//��������� �������� ������
        virtual float Area() = 0;//��������� ������� ������
        virtual float Get_perimetr() = 0;//���������� �������� ���������
        virtual float Get_area() = 0;//���������� �������� �������
        virtual void print() = 0;//������� �������������� ������ �������� ��������� � �������
};

#endif // FIGURE_H
