#ifndef RHOMBUS_H
#define RHOMBUS_H

#include "Parallelogram.h"

class Rhombus:public Parallelogram
{
    public:
        Rhombus(float a, float b, float c, float d, float h);
        float Area();
        float Get_area();//���������� �������� �������
        void print();//������� �������������� ������ �������� ��������� � �������
    private:
        float area;
        float height;
};

#endif // RHOMBUS_H
