#ifndef QUADRANGLE_H
#define QUADRANGLE_H

#include "Figure.h"

class Quadrangle:public Figure
{
    public:
        Quadrangle(float a, float b, float c, float d);
        float Perimetr();
        virtual void print();
        float Get_perimetr();
        virtual float Area() ;
        virtual float Get_area() ;
    protected:
        float storona1, storona2, storona3, storona4;
        float perimetr;
};

#endif // QUADRANGLE_H
