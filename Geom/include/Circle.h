#ifndef CIRCLE_H
#define CIRCLE_H

#include "Figure.h"

class Circle:public Figure
{
    public:
        Circle(float R);
        float Perimetr();//��������� ����� ����������
        float Area();//��������� �������
        float Get_radius();//���������� �������� �������
        float Get_perimetr();//���������� �������� �����
        float Get_area();//���������� �������� �������
        void print();//����� ����� ���������� � �������
    private:
        float radius;
        float length;
        float area;
};

#endif // CIRCLE_H
