#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Figure.h"

class Triangle:public Figure
{
    public:
        Triangle(float a, float b, float c);
        float Perimetr();//��������� ��������
        float Area();//��������� �������
        float Get_perimetr();//���������� �������� �����
        float Get_area();//���������� �������� �������
        void print();//����� ��������� � �������
    private:
        float storona1, storona2, storona3;
        float perimetr;
        float area;
};

#endif // TRIANGLE_H
