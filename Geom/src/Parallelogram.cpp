#include "Parallelogram.h"

#include <iostream>

Parallelogram::Parallelogram(float a, float b, float c, float d, float h):Quadrangle(a,b,c,d)
{
    height = h;
}

float Parallelogram::Area()
{
    area = height * storona1;
}

float Parallelogram::Get_area()
{
    return area;
}

void Parallelogram::print()
{
    std::cout << "Perimetr is " << perimetr << std::endl;
    std::cout << "Area is " << area << std::endl;
}
