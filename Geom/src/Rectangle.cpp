#include "Rectangle.h"
#include <iostream>

Rectangle::Rectangle(float a, float b, float c, float d, float h):Parallelogram(a,b,c,d,h)
{
    height = h;
}

float Rectangle::Area()
{
    area = storona1*storona3;
}

float Rectangle::Get_area()
{
    return area;
}

void Rectangle::print()
{
    std::cout << "Perimetr is " << perimetr << std::endl;
    std::cout << "Area is " << area << std::endl;
}
