#include <iostream>
#include "Circle.h"
#define PI 3.14

Circle::Circle(float R)
{
   radius = R;
}

float Circle::Perimetr()
{
    length = 2 * PI * radius;
    return length;
}

float Circle::Area()
{
        area = PI * radius * radius;
        return area;
}

float Circle::Get_radius()
{
    return radius;
}

float Circle::Get_perimetr()
{
    return length;
}

float Circle::Get_area()
{
    return area;
}

void Circle::print(){
    std::cout << "Length is " << length << std::endl;
    std::cout << "Area is " << area << std::endl;
}
