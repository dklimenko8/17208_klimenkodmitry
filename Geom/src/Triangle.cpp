#include "Triangle.h"
#include <iostream>
#include <cmath>

Triangle::Triangle(float a, float b, float c)
{
   storona1 = a;
   storona2 = b;
   storona3 = c;
}

float Triangle::Perimetr()
{
    perimetr = storona1 + storona2 + storona3;
    return perimetr;
}

float Triangle::Area()
{
        area = sqrt((perimetr/2) * ((perimetr/2) - storona1) * ((perimetr/2) - storona2) * ((perimetr/2) - storona3));
        return area;
}

float Triangle::Get_perimetr()
{
    return perimetr;
}

float Triangle::Get_area()
{
    return area;
}

void Triangle::print(){
    std::cout << "Perimetr is " << perimetr << std::endl;
    std::cout << "Area is " << area << std::endl;
}
