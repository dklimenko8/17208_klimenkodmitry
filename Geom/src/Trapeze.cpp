#include "Trapeze.h"
#include <iostream>

Trapeze::Trapeze(float a, float b, float c, float d, float h):Quadrangle(a,b,c,d)
{
    height = h;
}

float Trapeze::Area()
{
    area = height * (storona1 + storona2) / 2;
}

float Trapeze::Get_area()
{
    return area;
}

void Trapeze::print()
{
    std::cout << "Perimetr is " << perimetr << std::endl;
    std::cout << "Area is " << area << std::endl;
}
