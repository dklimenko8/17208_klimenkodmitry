#include "Rhombus.h"
#include <iostream>

Rhombus::Rhombus(float a, float b, float c, float d, float h):Parallelogram(a,b,c,d,h)
{
    height = h;
}

float Rhombus::Area()
{
    area = storona1*height;
}

float Rhombus::Get_area()
{
    return area;
}

void Rhombus::print()
{
    std::cout << "Perimetr is " << perimetr << std::endl;
    std::cout << "Area is " << area << std::endl;
}
